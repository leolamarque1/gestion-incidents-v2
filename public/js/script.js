// Boutton assignation

let link = document.querySelectorAll("#assign-link-js")


link.forEach( (element) =>{

  element.addEventListener('click', (event) => {event.preventDefault()
    let id = element.value
    assign(event,id)
  })
})

function assign(event,id) {

    httpRequest = new XMLHttpRequest() 
    httpRequest.open('GET', '/bug/Engineer/assign/' + id)
    httpRequest.send()
    httpRequest.onreadystatechange = () => {

      if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
          let data = JSON.parse(httpRequest.responseText)
          event.target.parentElement.innerHTML = data.nom_user
        }
      }
    }
}

let linkClose = document.querySelectorAll("#close-link-js")

linkClose.forEach( (element) =>{

  element.addEventListener('click', (event) => {event.preventDefault()
    let id = element.value
    close(event,id) 
  })
})

function close(event,id) {

  httpRequest = new XMLHttpRequest() 
  httpRequest.open('GET', '/bug/Engineer/close/' + id)
  httpRequest.send()
  httpRequest.onreadystatechange = () => {

    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        let data = JSON.parse(httpRequest.responseText)
        event.target.parentElement.innerHTML = data.date
      }
    }
  }
}