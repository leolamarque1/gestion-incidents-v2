<?php

namespace BugApp\Controllers;

use BugApp\Models\UserManager;
use BugApp\Models\Engineer;
use BugApp\Models\Recorder;
use BugApp\Controllers\abstractController;

class userController extends abstractController
{

    public function login(){

        // Si il existe des données postées,
        // vérifier que le login exite en base de données aux données saisies en base de données

        if(!empty($_POST)) {

            $email = $_POST['email'];

            $manager = new UserManager();

            $user = $manager->findByEmail($email);

            // Si oui : Vérifier que le mot de passe correspond

            if($user != null){

                $password = $_POST['password'];

                $check = $manager->checkPassword($password, $user);

                if ($check === true) {

                    // Si oui :

                    switch(get_class($user)){

                        case 'BugApp\Models\Recorder':                       
                            // Si l'utilisateur est un 'recorder', alors :
                            // - créer une session
                            $_SESSION['user'] = $user;
                            $_SESSION['type'] = Recorder::Name;
                            // - afficher la liste des incidents (vue Client)
                            header('Location:'.PUBLIC_PATH.'bug');

                        break;

                        case 'BugApp\Models\Engineer':                       
                        // Si l'utilisateur est un 'recorder', alors :
                        // - créer une session
                        $_SESSION['user'] = $user;
                        $_SESSION['type'] = Engineer::NAME;
                        // - afficher la liste des incidents (vue Ingenieur)
                        header('Location:'.PUBLIC_PATH.'bug');

                        break;

                    }  


                } else {

                    // Si non (le mot de passe ne correspond pas) :
                    // Il y a une erreur. Afficher le formulaire de login avec un commentaire

                    $error = "le mot de passe est incorrect";

                    $content = $this->render('src/Views/User/login', ['error' => $error]);

                    return $this->sendHttpResponse($content, 200);

                }


            }else {

                    // Si non (le login n'existe pas) :
                    // Il y a une erreur. Afficher le formulaire de login avec un commentaire

                    $error = "le login n'existe pas";

                    $content = $this->render('src/Views/User/login', ['error' => $error]);

                    return $this->sendHttpResponse($content, 200);

            }


        }else{

            // pas de données postées : afficher simplement le formulaire

            $content = $this->render('src/Views/User/login', ['error' => '']);

            return $this->sendHttpResponse($content, 200);

        }


    }

    public function logout(){

        session_destroy();
        unset($_SESSION);
        header('Location: ' . PUBLIC_PATH .'login');

    }

}