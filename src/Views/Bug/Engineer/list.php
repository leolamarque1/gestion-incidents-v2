<?php $bugs = $parameters['bugs']; ?>

<?php include(__DIR__ . './../../include/header.php'); ?>

<body>

<?php include(__DIR__ . './../../include/nav.php'); ?>

<div class="container">

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center blue-grey-text text-darken-4">Liste d'incidents</h1>
        </div>
    </div>

    <div class="section">

        <div class="row">
            <h5>Filtres</h5>
            <p class="filtre">
                <label>
                    <input type="checkbox" />
                    <span>Afficher uniquement les incidents non-clôturés.</span>
                </label>
            </p>
            <p class="filtre">
                <label>
                    <input type="checkbox" />
                    <span>Afficher uniquement les incidents que je me suis assignés.</span>
                </label>
            </p>
        </div>

        <table>
        <tbody id="list-bug-js">
        <?php foreach($bugs as $bug) {  ?>
          <tr>
            <td><?= $bug->getId();?></td>
            <td><?= $bug->getTitle();?></td>
            <td><?= $bug->getRecorder();?></td>
            <td><?php echo $bug->getCreatedAt()->format("d/m/Y");?></td>

            <td>
              <?php if($bug->getClosedAt() != null){ ?>
              <button class="waves-effect waves-light btn blue disabled">Clôturer</button>
              <?php }else{ ?>
              <button class="waves-effect waves-light btn blue" value="<?= $bug->getId();?>" id="cloturer"><i class="material-icons left"></i>Clôturer</button>
              <?php } ?>
            </td>

            <td><a class="waves-effect waves-light btn blue" href="<?= PUBLIC_PATH; ?>bug/show/<?=$bug->getId();?>" /><i class="material-icons left">   </i>Afficher</a></td>
            
            <td>
              <?php if($bug->getEngineer() != null){ ?>
              <button class="waves-effect waves-light btn blue"><i class="material-icons left">person</i><?= $bug->getEngineer(); ?></button>
              <?php }else{ ?>
              <button class="waves-effect waves-light btn blue" value="<?= $bug->getId();?>" id="assigner"><i class="material-icons left">person_add</i>Assigner</button>
              <?php } ?>
            </td>
          </tr>
          <?php } ?>

      </tbody>
        </table>
    </div>
</div>

<?php include( __DIR__.'./../../include/footer.php');?>

</body>

</html>
