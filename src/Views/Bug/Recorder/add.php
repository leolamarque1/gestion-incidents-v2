<?php

include( __DIR__.'./../../include/header.php');

?>

<body>

<?php include( __DIR__.'./../../include/nav.php');?>

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center blue-grey-text text-darken-4">Consignenr un incident</h1>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <a class="btn-floating btn-large waves-effect waves-light blue-grey darken-3" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons">arrow_back</i>Retour à la liste</a>
        </div>

        <div class="section">
            <!--   Form Section   -->
            <div class="row">

                <div class="row">
                    <form class="col s12" action="<?= PUBLIC_PATH; ?>bug/add" method="post">
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="titre" id="titre" type="text" class="validate" name="title">
                                <label class="active" for="titre">Titre</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="date"  id="date" type="date" class="validate" name="createdAt">
                                <label for="date">Date d'observation</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea placeholder="Description"  name="description" id="description" cols="30" rows="20"></textarea>
                                <label class="active" for="description">Description</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button class="btn" type="submit" name="submit">Enregistrer
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

<?php include( __DIR__.'./../../include/footer.php');?>