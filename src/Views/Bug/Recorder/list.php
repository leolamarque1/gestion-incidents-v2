<?php
$bugs = $parameters['bugs'];
?>


<?php 

    include(__DIR__.'./../../include/header.php');
    include(__DIR__.'./../../include/nav.php');
  
?>

<div class="container">

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center blue-grey-text text-darken-4">Liste d'incidents</h1>
        </div>
    </div>

    <div class="row">
        <a class="btn-floating btn-large waves-effect waves-light btn" href="<?= PUBLIC_PATH; ?>bug/add"><i class="material-icons">add</i>Consigner un incident</a>
    </div>


    <div class="section">

        <table>
            <tbody>

            <?php foreach ($bugs as $bug) { ?>
                <?php /** @var $bug \BugApp\Models\Bug */ ?>
                <tr>
                    <td><?= $bug->getId(); ?></td>
                    <td><?= $bug->getTitle(); ?></td>
                    <td><?= $bug->getCreatedAt()->format('d/m/Y'); ?></td>
                    <td><a class="waves-effect waves-light btn blue" href="<?= PUBLIC_PATH; ?>bug/show/<?=$bug->getId();?>" /><i class="material-icons left"></i>Afficher</a></td>
                    <td></td>
                </tr>

            <?php } ?>

            </tbody>
        </table>
    </div>
</div>

<?php include( __DIR__.'./../../include/footer.php');?>

</body>

</html>
