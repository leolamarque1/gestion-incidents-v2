<?php

namespace BugApp\Models;

class Bug {

    private $id;
    private $title;
    private $description;
    private $createdAt;
    private $closed;
    private $recorder;
    private $engineer;

    function __construct() {
        
    }
   

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $createdAt);
        
        $this->createdAt = $date;

        return $this;
    }

    /**
     * Get the value of closed
     */ 
    public function getClosedAt()
    {
        return $this->closed;
    }

    /**
     * Set the value of closed
     *
     * @return  self
     */ 
    public function setClosedAt($closedAt)
    {
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $closedAt);
        
        $this->closed = $date;

        return $this;
    }

    /**
     * @return Recorder
     */
    public function getRecorder()
    {
        return $this->recorder;
    }

    /**
     * @param Recorder $recorder
     */
    public function setRecorder($recorder)
    {
        $this->recorder = $recorder;

        return $this;
    }

    /**
     * @return Engineer
     */
    public function getEngineer()
    {
        return $this->engineer;
    }

    /**
     * @param Enginer $engineer
     */
    public function setEngineer($engineer)
    {
        $this->engineer = $engineer;

        return $this;
    }



}