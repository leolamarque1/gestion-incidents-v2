<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{
    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug  WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);

        // Retour
        return $bug;
    }

    public function findAll()
    {

        /// Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT id,title,bug.description,createdAt,closed,recorder_id, engineer_id FROM bug');
        $sth->execute();

        $bugs=[];

        while ($result = $sth->fetch(\PDO::FETCH_ASSOC)) {
            # code...
            // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["recorder"]);
        $bug->setEngineer($result["engineer"]);

         array_push($bugs, $bug);
     }
        return $bugs;
    }

    public function add(Bug $bug){

        // Ajout d'un incident en BDD

        $dbh = static::connectDb();

        $sql = "INSERT INTO bug(title, description, closed, createdAt) VALUES (:title, :description, :closed, :createdAt)";
        $sth = $dbh->prepare($sql);
        $sth->execute(["title" => $bug->getTitle(),"description" => $bug->getDescription(),"createdAt" => $bug->getCreatedAt()->format('Y-m-d H:i:s'),"closed" => null,]);

    }

    public function update(Bug $bug){

        // Update d'un incident en BDD
       $dbh = static::connectDb();

       $req = $dbh->prepare('UPDATE bug SET closed = :clotureDate WHERE id = '.$bug->getId());

       $req->execute(array('clotureDate' => date("Y-m-d H:i:s")));
    }

    public function EngineerId($id){

        $dbh = static::connectDb();

        $sth = $dbh->prepare('SELECT id FROM engineer WHERE engineer.user_id = :id_engineer');

        $sth->bindParam(':id_engineer', $id, \PDO::PARAM_INT);

        $sth->execute();

        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        return $result['id'];
    }

    public function assignation(Bug $bug, Engineer $assign){

        $dbh = static::connectDb();

        $req = $dbh->prepare('UPDATE bug SET engineer_id = :engineer WHERE id ='.$bug->getId());

        $req->execute(array('engineer' => $this->EngineerId($assign->getId())));

     }

}

    
